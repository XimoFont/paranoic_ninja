﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienManager : MonoBehaviour
{

    public Transform launchPos;
    public float xVariation;
    public Transform creationPos;
    public float timeLaunch;
    private bool explodedEnemies = false;


    public GameObject[] alienPrefabs;
    private AlienCache[] alienCaches;
    private float currentTime;
	public Transform[] launchPosvect;
	int actualpos;

    public static AlienManager instance;

    // Use this for initialization
    void Awake()
    {
        instance = this;
        Vector3 creationPosAlien = creationPos.position;
        alienCaches = new AlienCache[alienPrefabs.Length];

        for(int i = 0; i < alienPrefabs.Length; i++)
        {
            //Big Meteors;
            alienCaches[i] = new AlienCache(alienPrefabs[i], creationPosAlien, creationPos, 30);

            creationPosAlien.y += 1;
        }
    }

    void Update()
    {
		actualpos = Random.Range (0, 3);
        currentTime += Time.deltaTime;
        if(currentTime > timeLaunch)
        {
            int selected = Random.Range(0, alienPrefabs.Length);
			alienCaches[selected].GetAlien().LaunchAlien(launchPosvect[actualpos].position, new Vector2(Random.Range(-5, 5), Random.Range(-5, -1)), 20);
            currentTime -= timeLaunch;
        }
		if (SpaceManager.instance.clear1 == true && !explodedEnemies)
        {
            explodedEnemies = true;
            ExplodeAll ();
		}
    }

    public void LaunchAlien(int type, Vector3 mPosition, Vector2 mDirection, float mRotation)
    {
        alienCaches[type].GetAlien().LaunchAlien(mPosition, mDirection, mRotation);
    }

	public void ExplodeAll ()
	{
		for(int i = 0;i<alienCaches.Length;i++)
		{
			alienCaches [i].GetAlien ().ExplodeBoss ();
			alienCaches [i].GetAlien ().gameObject.SetActive (false);
		}
	}


}