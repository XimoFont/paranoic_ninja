﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kunai : MonoBehaviour {
    public AudioSource audioExplosion;
    public AudioSource audioLanzado;
    public ParticleSystem explosion;
    public float speed = 10;
    // Use this for initialization
    void Start () {
        audioLanzado.Play();
        explosion.Play();
    }
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector2.right * Time.deltaTime * speed);
	}
    void OnCollisionEnter2D(Collision2D other)
    {
        audioExplosion.Play();
        if (other.gameObject.tag == "Enemy"||other.gameObject.tag == "Boundary")
        {
            
            Destroy(gameObject);
        }
       
    }
}
