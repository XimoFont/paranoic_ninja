﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public float speed;
	public bool shooting = false;
	private Vector3 iniPos;
    void Awake(){
		iniPos = transform.position;
    }

	public void Shot(Vector3 position, float direction){
        transform.position = position;
		shooting = true;
		transform.rotation = Quaternion.Euler(direction,0, 0);
	}
	
	// Update is called once per frame
	protected void Update () {
		if (shooting) {
			transform.Translate (speed * Time.deltaTime, 0, 0);
		}
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Boundary")
        {

            Destroy(gameObject);
        }

    }

    public void Reset(){
        
		shooting = false;
	}
}
