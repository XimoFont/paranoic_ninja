﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float speed;
    private bool pared=false;
    private bool flipped = false;
    private Rigidbody2D rb2d = null;
    void Update ()
    {
       
        transform.Translate(Vector2.left * speed * Time.deltaTime);

        if (pared)
        {
            this.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        if (flipped)
        {
            this.transform.rotation = Quaternion.Euler(0, 180, 0);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Boundary")
        {
            Debug.Log("touch");
            flipped = true;
        }
        if (collision.tag == "Boundary2")
        {
            Debug.Log("touch");
            pared = true;

        }
        if (collision.tag == "Attack")
        {
            SpaceManager.instance.addHighscore(50);
            Debug.Log("touch");
            Dead();

        }
    }

    void Dead()
    {
        Destroy(this.gameObject);
    }
}
