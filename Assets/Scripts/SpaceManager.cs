﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class SpaceManager : MonoBehaviour {

    public static SpaceManager instance;
	public int highscore;
    public Text highscoreText;
    public int lifeplayer;
    public Text lifeText;
    public GameObject GameOver;
    public GameObject Pause;
    public GameObject Finish;
    public bool clear1=false;
    public bool lifeup=false;
    public GameObject Enemy;
    public GameObject bGround;
    public GameObject bgBoss
        ;

    // Use this for initialization
    void Start () {
        instance = this;
        highscore = 0;
        lifeplayer = 3;
        lifeText.text = "x" + lifeplayer.ToString();
        highscoreText.text = highscore.ToString("D5");
        bGround.gameObject.SetActive(true);
        bgBoss.gameObject.SetActive(false);
        Pause.gameObject.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pauseg();
        }

        if (highscore>=4000)
		{
            EnemyAppear();
            bgBossAppear();
            bGroundAppear();
        }
        if (highscore >= 9000 && clear1 != true)
        {
            clear1 = true;
        }
        if (clear1==true){
			Finishg ();
		}
    }



    public void addHighscore(int value)
    {
        highscore += value;
        highscoreText.text = highscore.ToString("D5");
    }


    public void sublive()
    {
        if(lifeplayer > 0)
        {
            lifeplayer--;
            lifeText.text = "X" + lifeplayer.ToString();     
        }
        else
        {
            GameOverg();
        
        }
    }

	public void addlive()
	{

		lifeplayer++;
		lifeText.text = "X" + lifeplayer.ToString();     

	}
    public void EnemyAppear()
    {
        Enemy.gameObject.SetActive(true);
    }
    public void bgBossAppear()
    {
        bgBoss.gameObject.SetActive(true);
    }
    public void bGroundAppear()
    {
        bGround.gameObject.SetActive(false);
    }



    public void Pauseg()
    {
        Pause.SetActive(!Pause.activeSelf);
        if(Pause.activeSelf == true)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

    }
    public void GameOverg()
    {
        GameOver.SetActive(!GameOver.activeSelf);


        if(GameOver.activeSelf == true)
        {
            Debug.Log("GameOver");
            SceneManager.LoadScene("GameOver");
        }
        else
        {
            Time.timeScale = 1;
        }
    }
    public void Finishg()
    {

            SceneManager.LoadScene("Finish");

    }

}
