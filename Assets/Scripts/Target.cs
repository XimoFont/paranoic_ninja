﻿using UnityEngine;

public class Target : MonoBehaviour
{
    // left and right marks
    public Transform left;
    public Transform right;
    private bool flipped = false;
    // speed
    public float speed = 1.0f;

    // current direction (false means to the left, true means to the right)
    bool dir = false;

    // Update is called once per frame
    void Update()
    {
        if (flipped == false)
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        if (flipped == true)
        {
            this.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        if (dir)
        {
            // go closer to the right one
            transform.position = Vector3.MoveTowards(transform.position,
                right.position,
                Time.deltaTime * speed);
            flipped = true;

            // reached it?
            if (transform.position == right.position)
                dir = !dir; // go to opposite direction next time
         
        }
        else
        {
            // go closer to the left one
            transform.position = Vector3.MoveTowards(transform.position,
                left.position,
                Time.deltaTime * speed);
            flipped = false;
            // reached it?
            if (transform.position == left.position)
                dir = !dir; // go to opposite direction next time
            


        }
    }
}
