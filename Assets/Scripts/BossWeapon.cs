﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWeapon : MonoBehaviour
{

    public GameObject shot;
    public Transform shotSpawn;
    public float delay;
    public float fireRate;
	public int maxShot;
	public static int actualShot;

	private void Start()
	{
		actualShot = 0;
        StartCoroutine("Fire");
        //InvokeRepeating("Fire", delay, fireRate);
	}

	IEnumerator Fire()
	{
        yield return new WaitForSeconds(delay);

        while(true)
        {
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);

            yield return new WaitForSeconds(fireRate);
        }
	}
}
