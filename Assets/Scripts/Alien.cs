﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien : MonoBehaviour
{

    public bool launched;
    protected Vector2 moveSpeed;
    protected float rotationSpeed;
    public ParticleSystem explosion;
    public GameObject graphics;
    private Collider2D alienCollider;
    public AudioSource audioExplosion;
    private Vector3 iniPos;
    private Transform target;
    private bool playerAlive = false;
    private float currentTime;
    public float timeLaunch = 4;
    void Start()
    {
        iniPos = transform.position;
        alienCollider = GetComponent<Collider2D>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void LaunchAlien(Vector3 position, Vector2 direction, float rotation)
    {
        transform.position = position;
        launched = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        moveSpeed = direction;
        rotationSpeed = rotation;
        graphics.gameObject.SetActive(true);
        alienCollider.enabled = true;
    }

    // Update is called once per frame
    protected void Update()
    {
        if(launched)
        {
            if (PlayerBehaviour.PlayerAlive())
            {
                transform.position = Vector2.MoveTowards(transform.position, target.transform.position, 4.0f * Time.deltaTime);
            }
            else
            {
                transform.Translate(moveSpeed.x * Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
                transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
            }
        }
        currentTime += Time.deltaTime;

        if (currentTime > timeLaunch)
        {
            playerAlive = true;
        }
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Bullet")
        {
            Explode();
        }
        else if(other.tag == "Player")
        {
            Explode();
        }
        else if(other.tag == "Finish")
        {
            Reset();
        }
    }

    protected void Reset()
    {
        transform.position = iniPos;
        launched = false;
        graphics.SetActive(true);
        alienCollider.enabled = true;
    }

    protected virtual void Explode()
    {
        audioExplosion.Play();
        launched = false;
        graphics.SetActive(false);
        alienCollider.enabled = false;
        explosion.Play();
        Invoke("Reset", 1);
        SpaceManager.instance.addHighscore(50);
    }
	public virtual void ExplodeBoss()
	{
		audioExplosion.Play();
		launched = false;
		graphics.SetActive(false);
		alienCollider.enabled = false;
		explosion.Play();
	}

   
}
