﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Finish : MonoBehaviour {
	public Text highscoreText;
	// Use this for initialization
	void Start () {
		highscoreText.text=SpaceManager.instance.highscore.ToString("D5");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
