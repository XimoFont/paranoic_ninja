﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    public float speed;
    public bool shooting = false;
    private Vector3 iniPos;
	private float currentTime;
	public float timeLaunch;

    public void Shot(Vector3 position, float direction)
    {
        transform.position = position;
        shooting = true;
        transform.rotation = Quaternion.Euler(0, 0, direction);
    }

    // Update is called once per frame
    protected void Update()
    {
		currentTime += Time.deltaTime;
		if (currentTime > timeLaunch) {
			Reset();
		}
        if(shooting)
        {
            transform.Translate(speed * Time.deltaTime, 0,  0);
        }
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Boundary" || other.gameObject.tag == "Player")
        {

            Destroy(gameObject);
        }

    }

    public void Reset()
    {
        shooting = false;
    }
}
