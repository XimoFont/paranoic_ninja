﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {
    // Use this for initialization
    void Awake()
    {
        // Obtenemos el Animator Controller para poder modificar sus variables

    }

    void OnCollisionEnter2D(Collision2D enemy)
    {
        Debug.Log("muerto");

        if (enemy.gameObject.tag =="player")
        {
            return;
        }
        if (enemy.gameObject.tag == "Bullet")
        {
            Explode();
        }

    }
    protected virtual void Explode()
    {
        Destroy(gameObject);
        SpaceManager.instance.addHighscore(500);
    }
}
