﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnemyBoss : MonoBehaviour {

    public GameObject graphics;
	private BoxCollider2D myCollider;
	public bool canShoot = true;
    public Slider enemyLife;
	private bool death = false;

	void Awake()
	{
		enemyLife.value=1.0f;
		myCollider = GetComponent<BoxCollider2D>();
		enemyLife.gameObject.SetActive(true);

	}

	// Update is called once per frame
	void Update () {
		Debug.Log (enemyLife.value);
		
	    if(enemyLife.value<=0 ){
		Death();
			
		}
		

	}
    public void OnCollisionEnter2D(Collision2D other)
    {
		if (other.gameObject.tag == "Bullet")
		{
            enemyLife.value -= 0.03f;      
		}
	}
	private void Explode()
	{
		graphics.SetActive(false);
		myCollider.enabled = false;
		canShoot = false;
	}

    private void Attack(){

	}	

	private void Death(){
		death=!death;
		Explode ();
		SpaceManager.instance.addHighscore(5000);
		enemyLife.gameObject.SetActive(false);
		gameObject.SetActive (false);
        Destroy(gameObject);
        

    }
}
