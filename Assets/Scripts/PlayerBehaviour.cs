﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {
    public ParticleSystem explosion;
    public AudioSource audio;
    private BoxCollider2D myCollider;
    static bool playerAlive = true;
    void Awake()
    {
        myCollider = GetComponent<BoxCollider2D>();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
		if (other.tag == "Meteors" || other.tag=="BulletBoss")
        {
            Explode();
        }
    }
    private void Explode()
    {
        playerAlive = false;
        SpaceManager.instance.sublive();
        myCollider.enabled = false;
        explosion.Play();
        audio.Play();
        Invoke("Reset", 2);
    }
    private void Reset()
    {
        playerAlive = true;
        myCollider.enabled = true;
    }
    static public bool PlayerAlive()
    {
        return playerAlive;
    }
}
